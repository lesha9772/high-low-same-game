<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="/css/app.css">
    <link rel="shortcut icon" type="image/png" href="/images/logo.png"/>
    <title>High, Low, Same</title>
</head>
<body>
<div id="vuapp">
    <app></app>
</div>
<script type="text/javascript" src="/js/vueapp.js"></script>
</body>
</html>
