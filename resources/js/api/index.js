import axios from 'axios';

var serverBaseUrl = '//' + document.location.hostname + ':' + location.port + '/api/';
const corsOptions = {};

export default {
    getCardsNewGame() {
        return axios.get(serverBaseUrl + 'cards/new-game', corsOptions);
    },

    sendAnswer(params) {
        return axios.post(serverBaseUrl + 'cards/check-answer', params);
    },

}
