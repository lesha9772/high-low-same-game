// require('./bootstrap');


import Vue from 'vue'
import App from './components/App'
import VueRouter from 'vue-router'
import VueHead from 'vue-head'
import VueI18n from 'vue-i18n'
import Notifications from 'vue-notification';
import VModal from 'vue-js-modal';
import imagePreloader from 'vue-image-preloader';
import router from './router';
import en from './lang/en.js';

Vue.use(VueHead)
Vue.use(VueRouter)
Vue.use(VueI18n)
Vue.use(Notifications);
Vue.use(VModal, { dialog: true });
Vue.use(imagePreloader)


// Vue.component('example-component', require('./components/ExampleComponent.vue').default);

// Ready translated locale messages
const messages = {
    en: en
};

// Create VueI18n instance with options
const i18n = new VueI18n({
    locale: 'en', // set locale
    messages, // set locale messages
});


const app = new Vue({
    i18n,
    el: '#vuapp',
    components: { App },
    router
});
