import api from '../api';
import modals from '../game/modal.js';


export default {
    userWinner(component) {
        component.gameClose = true;
    },

    userLosser(component) {
        component.gameClose = true;
    },

    restartGame(component) {
        component.startNewHand();
        component.gameStarted = false;
        component.giveOutCards();
    },

    closeGame(component) {
        component.startNewHand();
        component.gameStarted = false;
    },

    startNewHand(component) {
        component.cantTakeMore = false;
        component.gameClose = false;
        component.deck = [];
        component.player.hand = [];
        component.dealer.hand = [];
    },

    takeCard(component, position) {
        modals.loading(component);
        if (component.gameClose == true) {
            return false;
        }

        var params = new URLSearchParams();
        params.append('answer', position);
        params.append('deckId', component.deckId);

        api.sendAnswer(params)
            .then(response => {
                if (response.status === 200 && response.data.success === true) {
                    component.addCardTo(component.player);
                    component.deck = [];
                    component.dealer.hand = [];
                    component.deck.push(response.data.card);
                    component.addCardTo(component.dealer);

                    if (response.data.is_last_card === true) {
                        component.$modal.hide('dialog');
                        component.userWinner();
                    } else {

                        setTimeout(component.$modal.hide('dialog'), 500);
                    }
                } else if (response.status === 200 && response.data.success === false) {
                    component.$modal.hide('dialog');
                    component.userLosser();
                } else {
                    component.serverError();
                }

            })
            .catch(e => {
                component.errors.push(e);
            });
    },

    getCardsNewGame(component) {
        modals.loading(component);
        api.getCardsNewGame()
            .then(response => {
                if (response.status === 200 && response.data.success === true) {
                    component.deck.push(response.data.card);
                    component.deckId = response.data.deckId;

                    // Preload images
                    let countDeck = component.deck.length;
                    let imagesLoad = [];
                    for (let i = 0; i < countDeck; i++) {
                        imagesLoad.push(component.deck[i].image)
                    }
                    component.$imagePreload(imagesLoad); // element, loaded src, src index, loaded count, src list length, progress
                    component.gameStarted = true;
                    component.addCardTo(component.dealer);

                    setTimeout(component.$modal.hide('dialog'), 500);
                } else {
                    component.$modal.hide('dialog');
                    component.serverError();
                }
            })
            .catch(e => {
                component.errors.push(e);
            });
    }
}
