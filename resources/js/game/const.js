const HIGHER = 2;
const LOWER  = 1;
const SAME   = 0;

export default {
    HIGHER: HIGHER,
    LOWER:  LOWER,
    SAME:   SAME,
}