import VModal from 'vue-js-modal';

export default {
    loading(component) {
        component.$modal.show('dialog', {
            text: '<div class="loading-modal"><img class="spinner" src="/images/loading.gif"> <div class="message">Loading... Please wait</div></div>',
            buttons: []
        });

    },

    serverError(component) {
        component.$modal.show('dialog', {
            text: '<div class="loading-modal"> <div class="message">Server Error: Please Try Again Later</div> </div>',
            buttons: [
                {
                    title: 'Close',
                    handler: () => {
                        component.$modal.hide('dialog');
                    }
                },
                {
                    title: 'Try again',
                    handler: () => {
                        component.$modal.hide('dialog');
                        component.restartGame();
                    }
                }]
        });
    },

    userLosser(component){
        component.$modal.show('dialog', {
            text: '<div class="loading-modal"> <div class="message">You\'re Lose!</div> </div>',
            buttons: [
                {
                    title: 'Close',
                    handler: () => {
                        component.$modal.hide('dialog');
                        component.closeGame();
                    }
                },
                {
                    title: 'Try again',
                    handler: () => {
                        component.$modal.hide('dialog');
                        component.restartGame();
                    }
                }]
        });
    },

    userWinner(component){
        component.$modal.show('dialog', {
            text: '<div class="loading-modal"> <div class="message">You\'re Win!</div></div>',
            buttons: [
                {
                    title: 'Close',
                    handler: () => {
                        component.$modal.hide('dialog');
                        component.closeGame();
                    }
                }]
        });
    },

    restartUserGame(component){
        component.$modal.show('dialog', {
            text: '<div class="loading-modal"> <div class="message">Restart game ?</div> </div>',
            buttons: [
                {
                    title: 'Close',
                    handler: () => {
                        component.$modal.hide('dialog');
                    }
                },
                {
                    title: 'Restart',
                    handler: () => {
                        component.$modal.hide('dialog');
                        component.restartGame();
                    }
                }]
        });
    }

}
