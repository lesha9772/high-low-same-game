import Vue from 'vue'
import Router from 'vue-router'
import Game from '../components/Game'
Vue.use(Router)

/**
 * scrollBehavior
 * only available in html5 history mode
 * defaults to no scroll behavior
 * return false to prevent scroll
 *
 * @param to
 * @param from
 * @param savedPosition
 * @returns {*}
 */
const scrollBehavior = (to, from, savedPosition) => {
  if (savedPosition) {
    // savedPosition is only available for popstate navigations
    return savedPosition
  } else {
    const position = {}
    // new navigation, scroll to anchor by returning the selector
    if (to.hash) {
      position.selector = to.hash
      // specify offset of the element
      if (to.hash === '#anchor2') {
        position.offset = { y: 100 }
      }
    }
    // check if any matched route config has meta that requires scrolling to top
    if (to.matched.some(m => m.meta.scrollToTop)) {
      // cords will be used if no selector is provided or if the selector didn't match any element
      position.x = 0
      position.y = 0
    }
    // if the returned position is falsy or an empty object, will retain current scroll position
    return position
  }
};

export default new Router({
  mode: 'history',
  scrollBehavior,
  history: true,
  linkActiveClass: 'active', // active class for non-exact links.
  linkExactActiveClass: 'active', // active class for *exact* links.
  routes: [
    {
      path: '/',
      name: 'Game',
      component: Game,
      meta: { scrollToTop: true }
    }
  ]
})
