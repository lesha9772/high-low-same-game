export default {
    button: {
        start: 'Start',
        restart: 'Restart',
        higher: 'Higher',
        lower: 'Lower',
        same: 'Same',
        showAllCards: 'Show All Cards',
    }
};