## About Game
The player must then guess if the value of the next card is higher, lower or the same as the card shown. If the player is correct, the game continues. If the player is wrong, the game is over. The goal is for the player to guess correct as many times as possible. Although improbable, if the player guesses all cards in the deck correct the player completes the game and �wins�.

## Install
In terminal write

```
1) composer install
```
```
2) npm install
```

```
3) cp .env.example .env
```

```
4) php artisan key:generate
```

```
5) docker network create game
```

```
6) docker-compose up
```


```
7) docker exec -t container_game_web  php /code/artisan migrate
```



Open in browser link http://localhost:8081/
