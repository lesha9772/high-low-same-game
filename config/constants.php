<?php

return [
    'URL_CREATE_NEW_SHUFFLE' => 'https://deckofcardsapi.com/api/deck/new/shuffle/',
    'URL_DRAW_CARD' => 'https://deckofcardsapi.com/api/deck/<<deck_id>>/draw/',
    'URL_RESHUFFLE_CARDS' => 'https://deckofcardsapi.com/api/deck/<<deck_id>>/shuffle/',
    'DEFAULT_DESK_COUNT' => 52,
    'ANSWER_SAME' => 0,
    'ANSWER_LOW' => 1,
    'ANSWER_HIGH' => 2,
    'SHUFFLE_STATUS_NEW' => 0,
    'SHUFFLE_STATUS_LOS' => 1,
    'SHUFFLE_STATUS_WIN' => 2
];