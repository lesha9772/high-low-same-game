<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Config;

class CreateShuffleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shuffle', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('deckId');
            $table->integer('count_cards')->default(0);
            $table->integer('current_card');
            $table->enum('status', [
                Config::get('constants.SHUFFLE_STATUS_NEW'),
                Config::get('constants.SHUFFLE_STATUS_LOS'),
                Config::get('constants.SHUFFLE_STATUS_WIN')])
                ->default(Config::get('constants.SHUFFLE_STATUS_NEW'));
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shuffle');
    }
}
