<?php

namespace App\Http\Controllers\Api;

use App\Cards;
use App\Http\Controllers\Controller;
use App\DeckofcardsApi;
use App\Shuffle;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

class CardsController extends Controller
{

    /**
     * Create new Game
     * get new Shuffle
     * @return \Illuminate\Http\JsonResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function newGame()
    {
        $result = ['success' => false];
        $deckofcardsApi = new DeckofcardsApi();
        $deckofcardsApi->newShuffle();
        $shuffleId = $deckofcardsApi->getShuffleId();
        $shuffleModel = Shuffle::select('id', 'count_cards', 'current_card', 'deckId')->where('id', $shuffleId)->first();
        $card = $shuffleModel->current_card_data;

        if (!empty($card)) {
            $result = [
                'deckId' => $shuffleModel->deckId,
                'shuffleId' => $shuffleModel->id,
                'card' => $card,
                'success' => true];
        } else {
            $result = ['success' => false];
        }

        return response()->json($result);
    }

    /**
     * Check user answer
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function checkAnswer(Request $request)
    {
        $answer = $request->post('answer');
        $deckId = $request->post('deckId');
        $result = ['success' => false, 'card' => '', 'is_last_card' => false];
        $correctly = false;
        $shuffleModel = Shuffle::select('id', 'count_cards', 'current_card', 'deckId')->where('deckId', $deckId)->first();
        $cardCurrent = $shuffleModel->current_card_data;
        $cardNext = $shuffleModel->next_card_data;
        $cardLast = $shuffleModel->last_card_data;

        if ($cardNext->id == $cardLast->id) {
            $result ['is_last_card'] = true;
        }
        switch ($answer):
            case Config::get('constants.ANSWER_SAME'):
                if ($cardCurrent->value === $cardNext->value) {
                    $correctly = true;
                }
                break;
            case Config::get('constants.ANSWER_LOW'):
                if ($cardNext->value < $cardCurrent->value) {
                    $correctly = true;
                }
                break;
            case Config::get('constants.ANSWER_HIGH'):
                if ($cardNext->value > $cardCurrent->value) {
                    $correctly = true;
                }
                break;
        endswitch;

        /**
         * if user right - save next card as current
         */
        if ($correctly === true) {
            $result ['success'] = true;
            $result ['card'] = $cardNext;
            $shuffleModel->current_card = $cardNext->number;
            $shuffleModel->save();
        }

        return response()->json($result);
    }

    public function index()
    {
        $cards = [];
        return response()->json($cards);
    }
}
