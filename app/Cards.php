<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Cards
 *
 * @package App
 */
class Cards extends Model
{
    protected $table = 'cards';
}
