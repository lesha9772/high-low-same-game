<?php

namespace App;

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

/**
 * Class DeckofcardsApi
 * create shuffle, get lists of card
 *
 * @package App
 */
class DeckofcardsApi
{

    private $deckCount;
    private $deckId;
    private $shuffleId;

    /**
     * @var array
     */
    private $cards = [];


    /**
     * DeckofcardsApi constructor.
     *
     * @param int $deckCount
     * @param int $cardsCount
     * @throws GuzzleException
     */
    public function __construct(int $deckCount = 0)
    {
        if ($this->deckCount > 0) $this->deckCount = $deckCount;
        else $this->deckCount = Config::get('constants.DEFAULT_DESK_COUNT');
    }

    /**
     * @return bool
     * @throws GuzzleException
     */
    public function newShuffle()
    {
        $client = new Client();
        $request = $client->request('GET', Config::get('constants.URL_CREATE_NEW_SHUFFLE'), [
            'form_params' => [
                'deck_count' => $this->deckCount
            ]
        ]);

        $requestCode = $request->getStatusCode();
        if ($requestCode === 200) {
            $dataNewShuffleJson = $request->getBody();
            $dataNewShuffle = json_decode($dataNewShuffleJson);

            if ($dataNewShuffle->success === true) {
                $this->deckId = $dataNewShuffle->deck_id;
                $this->getCardsData();
                $this->saveCards();
            }

        }

        return true;
    }


    /**
     * Get card list and fix value for some cards
     * @throws GuzzleException
     */
    private function getCardsData()
    {
        $countReplace = 1;
        $uri = str_replace('<<deck_id>>', $this->deckId, Config::get('constants.URL_DRAW_CARD'), $countReplace);
        $client = new Client();
        $request = $client->request('GET', $uri, [
            'query' => [
                'count' => $this->deckCount
            ]
        ]);

        $requestCode = $request->getStatusCode();
        if ($requestCode === 200) {
            $dataNewCardsJson = $request->getBody();
            $dataNewCards = json_decode($dataNewCardsJson);
            if ($dataNewCards->success === true && $dataNewCards->deck_id === $this->deckId) {
                $cards = $dataNewCards->cards;
                foreach ($cards as &$cardOne) {
                    $cardOne->value = str_replace('JACK', '11', $cardOne->value);
                    $cardOne->value = str_replace('QUEEN', '12', $cardOne->value);
                    $cardOne->value = str_replace('KING', '13', $cardOne->value);
                    $cardOne->value = str_replace('ACE', '1', $cardOne->value);
                }
                $this->cards = $cards;
            }
        }
    }


    private function saveCards()
    {
        // save shuffle
        $shuffle = new Shuffle();
        $shuffle->deckId = $this->deckId;
        $shuffle->count_cards = $this->deckCount;
        $shuffle->current_card = 1;
        $shuffle->save();
        $this->shuffleId = $shuffle->id;


        // save cards
        $dataSet = [];
        for ($i = 0; $i < $this->deckCount; $i++) {
            $dataSet[] = [
                'number' => ($i + 1),
                'value' => $this->cards[$i]->value,
                'code' => $this->cards[$i]->code,
                'suit' => $this->cards[$i]->code,
                'image' => $this->cards[$i]->image,
                'shuffle_id' => $shuffle->id,
                'deck_id' => $shuffle->deckId,
            ];
        }

        DB::table('cards')->insert($dataSet);
    }


    /**
     * @return array
     */
    public function getCards()
    {
        return $this->cards;
    }

    public function getDeckId()
    {
        return $this->deckId;
    }

    public function getShuffleId()
    {
        return $this->shuffleId;
    }

}
