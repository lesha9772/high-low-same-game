<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Shuffle
 *
 * @package App
 */
class Shuffle extends Model
{
    protected $table = 'shuffle';

    /**
     * @return mixed
     */
    public function getCurrentCardDataAttribute(){

        return Cards::select('id','number', 'value', 'code', 'suit', 'image')->where('shuffle_id', $this->id)->where('number', $this->current_card)->first();
    }

    /**
     * @return mixed
     */
    public function getNextCardDataAttribute(){

        return Cards::select('id','number', 'value', 'code', 'suit', 'image')->where('shuffle_id', $this->id)->where('number', $this->current_card+1)->first();
    }

    /**
     * @return mixed
     */
    public function getLastCardDataAttribute(){

        return Cards::select('id','number', 'value', 'code', 'suit', 'image')->where('shuffle_id', $this->id)->orderBy('id', 'DESC')->first();
    }
}
